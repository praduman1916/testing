*** Settings ***
Library  SeleniumLibrary
*** Test Cases ***
Navigation test cases
    open browser  https://www.google.com  chrome
    ${location}=   get location
    log to console  ${location}
    go to  https://www.bing.com
    ${location2}=  get location
    log to console  ${location2}
    go back
    ${location3}=  get location
    log to console  ${location3}