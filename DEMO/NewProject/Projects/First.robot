#All section in robot
*** Settings ***
#In this section we add all the libray
Library  SeleniumLibrary
*** Variables ***
#In this section we define all the variable
${LOGIN URL}      https://opensource-demo.orangehrmlive.com/
${BROWSER}        Chrome
*** Test Cases ***
# In this section we we=rite multiple test  test cases
# Title of the test cases
FirstTestCase
     open browser    ${LOGIN URL}    ${BROWSER}
SecondTestCase
      input text  id:txtUsername   Admin
      input text  id:txtPassword  admin123
ThirdTestCase
     click element  xpath://input[@id='btnLogin']
     close browser
#Four
#     click link  xpath://*[@id="search"]/div[1]/div[1]/div/span[3]/div[2]/div[2]/div/div/div/div/div/div[2]/div/div/div[1]/h2/a
#     #close browser
#*** Keywords ***
##in this we define our own keyword