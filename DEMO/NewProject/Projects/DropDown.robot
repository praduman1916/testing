*** Settings ***
*** Settings ***
Library  SeleniumLibrary
*** Variables ***
${url}  https://www.tutorialspoint.com/selenium/selenium_automation_practice.htm
${browser}   Chrome
*** Test Cases ***
Tc1:Select checkbox
    open browser  ${url}  ${browser}
#   / maximize browser window
#    //set selenium speed 2seconds
    input text  name:firstname  Praduman
    input text  name:lastname  Kushwaha
    Select Radio Button  sex   Female
    Select Radio Button  exp   3
    input text  xpath://*[@id="mainContent"]/div[4]/div/form/table/tbody/tr[5]/td[2]/input  22/3/22
    Select Checkbox  Automation Tester
    sleep  4
    select from list by label  continents  Africa
    sleep  4
    select from list by index  continents  1

