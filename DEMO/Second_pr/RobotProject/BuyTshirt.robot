*** Settings ***
Library  SeleniumLibrary
*** Variables ***
${LOGIN URL}      http://www.amazon.com
${BROWSER}        chrome
*** Test Cases ***
TestCases
          open browser    ${LOGIN URL}    ${BROWSER}
         # input text  id:twotabsearchtextbox  t-shirt
          input text  //input[@id='twotabsearchtextbox']  t-shirt
          click element  xpath://*[@id="nav-search-submit-button"]
          click link   xpath://*[@id="search"]/div[1]/div[1]/div/span[3]/div[2]/div[3]/div/div/div/div/div/div/div[2]/div[1]/h2/a
          click element  xpath://input[@id='add-to-cart-button']
         # click element  xpath://span[@id='sw-gtc']
         click element  xpath://input[@name='proceedToRetailCheckout']
         input text  xpath://input[@id='ap_email']   8009711198
         click element  xpath://span[@id='continue']
*** Keywords ***
