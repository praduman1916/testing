*** Settings ***
Library  SeleniumLibrary
*** Variables ***
${browser}  Chrome
${url}  https://demo.nopcommerce.com/
*** Test Cases ***
TestingInputBox
    open browser  ${url}  ${browser}
    title should be  nopCommerce demo store
    click link  xpath://a[@class='ico-login']
    ${"Email_var"}  set variable  id:Email
     element should be visible   ${"Email_var"}
     element should be enabled    ${"Email_var"}
     input text  ${"Email_var"}   praduman.kushwaha.20.1@mountblue.tech
#     sleep  5
#     clear element text  ${"Email_var"}
#     sleep   5
#     close browser
    ${"password}  set variable  id:Password
     element should be visible  ${"password}
     element should be enabled   ${"password}
     input text  ${"password}  8009711198
     //clear element text  ${"password}