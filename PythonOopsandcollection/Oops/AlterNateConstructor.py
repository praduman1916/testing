class Employee:
    # constructor create
    increment=3
    def __init__(self,fname,lname,salary):
        self.fname = fname
        self.lname = lname
        self.salary = salary
        self.increment= 2
    def incrementSalary(self):
        self.salary=int(self.salary * Employee.increment)
        # self.salary=int(self.salary * self.increment)
    @classmethod
    def from_str(cls,emp_string):
        fname,lname,salary = emp_string.split("-")
harry=Employee("Harry","Kumar",33000)
rohan=Employee("rohan","das",30000)
lovish = Employee.from_str("lovish-das-2000")
print(lovish.salary)