# import random
# # print(random.randrange(1, 10))
# s=random.randrange(1,100)
# a = """Lorem ipsum dolor sit amet,
# consectetur adipiscing elit,
# sed do eiusmod tempor incididunt
# ut labore et dolore magna aliqua."""
# print(a)
# print(s)
# a = "Hello, World!"
# print(a.replace("H",'h'))
# age = 36
# txt = "My name is John, and I am {}"
# print(txt.format(age))
# quantity = 3
# itemno = 567
# price = 49.95
# myorder = "I want {} pieces of item {} for {} dollars."
# print(myorder.format(quantity, itemno, price))
# x = 20.0
# print(isinstance(x, float))
# thislist = ["apple", "banana", "cherry"]
# print(thislist[0:2])

# t = ("apple", "banana", "cherry","banana")
# print(t[1:3])
# print(t)
# thislist = ["apple", "banana", "cherry"]
# thislist.insert(2, "watermelon")
# print(thislist)
# thislist = ["apple", "banana", "cherry"]
# thistuple = ("kiwi", "orange")
# thislist.extend(thistuple)
# print(thislist)
# thislist = ["apple", "banana", "cherry"]
# for i in range(len(thislist)):
#     print(thislist[i])
# fruits = ["apple", "banana", "cherry", "kiwi", "mango"]
#
# newlist = [x for x in fruits if "a" in x]
#
# print(newlist)
# fruits = ["apple", "banana", "cherry", "kiwi", "mango"]
# newlist = [x.upper() for x in fruits]
# print(newlist)
#sort in decending order
# thislist = ["orange", "mango", "kiwi", "pineapple", "banana"]
# thislist.sort(reverse = True)
# print(thislist)
# def myfunc(n):
#   return abs(n - 50)
# thislist = [100, 50, 65, 82, 23]
# thislist.sort(key = myfunc)
# print(thislist)
# thislist = ["banana", "Orange", "Kiwi", "cherry"]
# thislist.sort(key = str.lower)
# print(thislist)
# l1=[1,3,3,4,5]
# l2=['q','y']
# l3=l1.extend(l2)
# print(l1)
# p=l1.copy()
# print(p)
