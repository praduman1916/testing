class Employee:
    # constructor create
    increment=3
    def __init__(self,fname,lname,salary):
        self.fname = fname
        self.lname = lname
        self.salary = salary
        self.increment= 2
    def incrementSalary(self):
        self.salary=int(self.salary * Employee.increment)
        # self.salary=int(self.salary * self.increment)
    @classmethod   #need of class variable
    def changeIncrement(cls,amount):
        cls.increment=amount
harry=Employee("Harry","Kumar",33000)
rohan=Employee("rohan","das",30000)
print(harry.salary)
print(rohan.salary)
Employee.changeIncrement(2)
harry.incrementSalary()
print(harry.salary)