class Employee:
    # constructor create
    increment=3
    def __init__(self,fname,lname,salary):
        self.fname = fname
        self.lname = lname
        self.salary = salary
        self.increment= 2
    def incrementSalary(self):
        self.salary=int(self.salary * Employee.increment)
        # self.salary=int(self.salary * self.increment)
harry=Employee("Harry","Kumar",33000)
rohan=Employee("rohan","das",30000)
print(harry.__dict__)
print(harry.salary,rohan.lname)
harry.incrementSalary()
print(harry.salary)
rohan.incrementSalary()
print(rohan.salary)
print(Employee.__dict__)
print(rohan.__dict__)
print(harry.__dict__)