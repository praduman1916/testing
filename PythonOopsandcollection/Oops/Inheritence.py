class Employee:
    # constructor create
    increment=3
    def __init__(self,fname,lname,salary):
        self.fname = fname
        self.lname = lname
        self.salary = salary
        self.increment= 2
    def incrementSalary(self):
        self.salary=int(self.salary * Employee.increment)
        # self.salary=int(self.salary * self.increment)
    @classmethod   #need of class variable
    def changeIncrement(cls,amount):
        cls.increment=amount
    @staticmethod
    def isOpen(day):
        if day =="sunday":
            return True
        else:
            return False
class programmer(Employee):
    def __init__(self,fname,lname,salary,prolang,exp):
        super().__init__(fname,lname,salary)
        self.prolang=prolang
        self.exp=exp
    def incrementSalary(self):
        self.salary=int(self.salary * Employee.increment+1000)
obj=programmer("Harray","Das",999,"python",4)
print(obj.fname,obj.lname,obj.prolang)
obj.incrementSalary()
print(obj.salary)

