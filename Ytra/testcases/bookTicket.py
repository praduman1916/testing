from time import sleep
from selenium import webdriver
from selenium.webdriver import Keys
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
# from selenium.webdriver.support.wait import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
class ytra:
    def travel_details(self):
        driver=webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        driver.implicitly_wait(10)
        driver.get("https://www.yatra.com/")
        driver.maximize_window()
        start_travel=driver.find_element(By.ID,"BE_flight_origin_city")
        start_travel.click()
        start_travel.send_keys("Lucknow")
        start_travel.send_keys(Keys.ENTER)
        # search_result=driver.find_elements(By.XPATH,"//div[@class='ac_results dest_ac']")
        search_result=driver.find_elements(By.XPATH,"//div[@class='viewport']//div[1]/li")
        print("All cites:")
        for cities in search_result:
            print(cities.text)
        for result in search_result:
            if "Kempegowda International" in result.text:
                result.click()
                break
        #sleep(5)
        wait=WebDriverWait(driver,10)
        wait.until(EC.element_to_be_clickable((By.XPATH,"//input[@id='BE_flight_origin_date']"))).click()
        sleep(4)
        all_detes=wait.until(EC.element_to_be_clickable((By.XPATH,"//div[@id='monthWrapper']//tbody//td[@class!='inActiveTD']")))\
        .find_elements(By.XPATH,"//div[@id='monthWrapper']//tbody//td[@class!='inActiveTD']")
        for alldates in all_detes:
            if alldates.get_attribute("data-date") == "22/08/2022":
                alldates.click()
                break
        driver.find_element(By.XPATH,"//input[@value='Search Flights']").click()
        sleep(5)
obj=ytra()
obj.travel_details()
