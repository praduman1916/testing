# ✅ get()- Returns the value for specified key in the dictionary
# ✅ keys()- Returns a copy of dictionaries key
# ✅ items()- Returns a copy for dictionaries key value pair
# ✅ values()- Returns a copy of the values in the dictionaries
# ✅ pop()- Removes the item with the specified key
# ✅ popitem()- Remove the arbitrary key:value pair
# ✅ item()- Adds the specified key-value pairs to dictionary
# ✅ copy()- Returns a copy of the dictionary
# ✅ clear()- Removes all the items of the dictionary
dict1={"fruit":"mango","country":"India","city":"blr"}
dict2={1:10,2:20,"country":"india"}
# print(dict1["country"])
# dict2.pop(2)
# print(dict2)
# print(dict1.get("city"))
print(dict2.keys())
print(dict1.items())
print(dict2.values())

