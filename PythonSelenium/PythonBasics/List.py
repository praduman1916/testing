# ✔️ list.append - Add an item to the end of the list.
# ✔️ list.insert - Insert an item at a given position.
# ✔️ list.remove - Remove the first item from the list whose value is equal to x.
# ✔️ list.pop - Remove the item at the given position in the list, and return it.
# ✔️ list.index - Return zero-based index in the list of the first item whose value is equal to x.
# ✔️ list.count - Return the number of times x appears in the list.
# ✔️ list.sort - Sort the items of the list in place.
# ✔️ list.reverse - Reverse the elements of the list in place.
# ✔️ list.copy - Return a shallow copy of the list.
# ✔️ list.clear - Remove all items from the list.
# l=[1,2,4,6,3,8,9]
# # print(l[1:3])
# cities=["delhi","Lucknow","Mumbai","lko"]
# # cities.append("Agara")
# # print(cities)
# # cities.insert(2,"Gkp")
# # print(cities)
# # cities.remove("Mumbai")
# print(cities)
# cities.pop(0)
# print(cities)
# print(cities.count("lko"))
# print(cities.index("lko"))
# cities.sort()
# print(cities)
# cities.reverse()
# print(cities)
# # upadatedList=cities.clear()
# # print(upadatedList)
# print(len(cities))
#####Set###
s={1,2,3,1,3,4,2}
print(1 in s)
print(s)