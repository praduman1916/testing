from time import sleep
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


class rightanddoubleclick:
    def rdclick(self):
        driver=webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        driver.implicitly_wait(5)
        driver.get("https://demo.guru99.com/test/simple_context_menu.html")
        action=ActionChains(driver)
        right = driver.find_element(By.XPATH,"//span[@class='context-menu-one btn btn-neutral']")
        action.context_click(right).perform()
        sleep(2)
        copyele=driver.find_element(By.XPATH, "//span[normalize-space()='Copy']")
        copyele.click()
        sleep(3)
        driver.switch_to.alert.accept()
        sleep(3)
        double=driver.find_element(By.XPATH, "//button[normalize-space()='Double-Click Me To See Alert']")
        action.double_click(double).perform()
        sleep(2)
obj=rightanddoubleclick()
obj.rdclick()