from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
class HiddenElement:
    def hidden(self):
        driver=webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        driver.maximize_window()
        driver.get("https://www.w3schools.com/howto/howto_js_toggle_hide_show.asp")
        # driver.get("https://www.yatra.com/hotels")
        ele=driver.find_element(By.XPATH,"//div[@id='myDIV']").is_displayed()
        print(ele)
        driver.find_element(By.XPATH,"//button[normalize-space()='Toggle Hide and Show']").click()
        ele = driver.find_element(By.XPATH, "//div[@id='myDIV']").is_displayed()
        print(ele)
        sleep(2)
    def ytraRemove(self):
        driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        driver.maximize_window()
        driver.get("https://www.yatra.com/hotels")
        driver.find_element(By.XPATH,"//span[normalize-space()='Hotels']").click()
        sleep(1)
        driver.find_element(By.XPATH,"//label[normalize-space()='Traveller and Hotel']").click()
        sleep(1)
        driver.find_element(By.XPATH,"//body[1]/div[2]/div[1]/section[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[3]/div[1]/div[1]/span[2]").click()
        sleep(1)
        ele1=driver.find_element(By.XPATH,"//select[@class='ageselect']").is_displayed()
        print(ele1)
        driver.find_element(By.XPATH,"//body[1]/div[2]/div[1]/section[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[3]/div[1]/div[1]/span[1]").click()
        ele1 = driver.find_element(By.XPATH, "//select[@class='ageselect']").is_displayed()
        print(ele1)

obj=HiddenElement()
# obj.hidden()
obj.ytraRemove()