from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from time import sleep


class windohandle:
    def handlewin(self):
        driver=webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        driver.maximize_window()
        driver.get("https://www.yatra.com/")
        parent = driver.current_window_handle
        print(parent)
        driver.find_element(By.XPATH,"//img[@alt='Flat 16% OFF (upto Rs.2,022)']").click()
        sleep(2)
        child=driver.window_handles
        print(child)
        for handle in child:
            if(handle !=parent):
                driver.switch_to.window(handle)
                driver.find_element(By.XPATH,"//a[contains(text(),'My Account')]").click()
                sleep(3)
                driver.close()
        sleep(3)
obj=windohandle()
obj.handlewin()

