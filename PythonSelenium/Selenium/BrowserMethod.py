from time import sleep

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
class BrowserOpreation:
    def browserAllMethods(self):
        driver=webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        driver.maximize_window()
        driver.get("https://www.wakefit.co")
        print(driver.title)
        print(driver.current_url)
        # driver.minimize_window()
        # sleep(3)
        # driver.fullscreen_window()
        driver.find_element(By.CSS_SELECTOR,"div[class='header-container'] li:nth-child(1) span:nth-child(1)").click()
        sleep(2)
        driver.find_element(By.XPATH,"//a[normalize-space()='Wakefit Mattress']").click();
        sleep(3)
        driver.refresh()
        sleep(3)
        driver.back()
        sleep(3)
        driver.forward()
        sleep(20)
obj=BrowserOpreation()
obj.browserAllMethods()