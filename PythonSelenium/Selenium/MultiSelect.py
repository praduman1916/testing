from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from time import sleep
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from webdriver_manager.chrome import ChromeDriverManager


class multidroupDown:
    def multiSelect(self):
        driver=webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        driver.maximize_window()
        driver.implicitly_wait(10)
        driver.get("https://www.tutorialspoint.com/selenium/selenium_automation_practice.htm")
        sleep(5)
        grp=driver.find_element(By.XPATH,"//select[@name='selenium_commands']")
        all=Select(grp)
        all.select_by_visible_text("Browser Commands")
        sleep(5)
obj=multidroupDown()
obj.multiSelect()
