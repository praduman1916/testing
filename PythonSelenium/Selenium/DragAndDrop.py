from time import sleep
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
class dragAndDroup:
    def dragDroup(self):
        driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        driver.get("https://jqueryui.com/droppable/")
        driver.maximize_window()
        driver.implicitly_wait(10)
        driver.switch_to.frame(driver.find_element(By.XPATH, "//iframe[@class='demo-frame']"))
        ele1 = driver.find_element(By.ID, "draggable")
        ele2 = driver.find_element(By.ID, "droppable")
        # ActionChains(driver).drag_and_drop(ele1, ele2).perform()
        ActionChains(driver).drag_and_drop_by_offset(ele1,30,40).perform()
        sleep(3)
obj = dragAndDroup()
obj.dragDroup()
