from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from time import sleep

class handleAlert:
    def Alerthandle(self):
        driver=webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        driver.maximize_window()
        driver.get("https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_confirm")
        driver.implicitly_wait(10)
        driver.switch_to.frame("iframeResult")
        driver.find_element(By.CSS_SELECTOR, "button[onclick='myFunction()']").click()
        sleep(2)
        driver.switch_to.alert.accept()
obj=handleAlert()
obj.Alerthandle()