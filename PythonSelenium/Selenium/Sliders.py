from time import sleep

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


class sliders:
    def priceSlider(self):
        driver=webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        driver.get("https://www.wakefit.co/home2/")
        driver.maximize_window()
        driver.implicitly_wait(10)
        action = ActionChains(driver)
        matterts=driver.find_element(By.XPATH,"//span[normalize-space()='Mattress']")
        action.move_to_element(matterts).perform()
        driver.find_element(By.XPATH,"//a[normalize-space()='Wakefit Mattress']").click()
        sleep(3)
        left=driver.find_elements(By.CSS_SELECTOR,".ant-slider-handle.ant-slider-handle-1")
        right=driver.find_elements(By.CSS_SELECTOR,".ant-slider-handle.ant-slider-handle-2")
        # ActionChains(driver).drag_and_drop_by_offset(left,50,0).perform()
        ActionChains(driver).click_and_hold(left).pause(2).move_by_offset(30,0).release().perform()
        # ActionChains(driver).move_to_element(left).pause(1).click_and_hold(left).move_by_offset(50,0).release().perform()


obj=sliders()
obj.priceSlider()