from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
class pronmptAlert:
    def handleAlert(self):
        driver=webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        driver.maximize_window()
        driver.implicitly_wait(5)
        driver.get("https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_prompt")
        driver.switch_to.frame("iframeResult")
        sleep(3)
        driver.find_element(By.XPATH,"//button[normalize-space()='Try it']").click()
        sleep(3)
        # Alert(driver).accept();
        driver.switch_to.alert.send_keys("hi,how are you")
        sleep(3)
        driver.switch_to.alert.accept()
obj=pronmptAlert()
obj.handleAlert()

