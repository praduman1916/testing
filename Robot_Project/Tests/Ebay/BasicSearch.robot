*** Settings ***
Documentation  Basic Search Functionality
Resource  ../../Resources/CommonFunctionality.robot
Resource  ../../Resources/DefinedKewords.robot


TEST TEMPLATE   TEST AUTOMATE



*** Variables ***
${URL}  https://www.ebay.com
${BROWSER}  chrome


*** Test Cases ***
Varify Basic Search functinality for eBay


*** Keywords ***
TEST AUTOMATE
    [Documentation]  This test cases verify the basic search
    [Tags]  Functonal
    Start test cases
    Varify Serch Result
    Filter Result By Condition
    Scroll Page
    Chose Product
    Finish test cases

#Add To Cart
#   Switch Window  title:Apple iPhone 12 - 64GB - Black - T-Mobile Locked - CRACKED BACK + CRACKED SCREEN + eBay
#   click element  xpath://a[@id='isCartBtn_btn']
