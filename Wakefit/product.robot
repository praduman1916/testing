*** Settings ***
Library  SeleniumLibrary
*** Variables ***
${URL}  https://qa.wakefit.co/home2/
${BROWSER}  Chrome
*** Test Cases ***
All Test Cases
    TC1:Open Browser
    TC2:See Product
    TC3:Chose product
    TC4:Scroll
    TC5:See product PDP
    TC6:Add to cart
*** Keywords ***
TC1:Open Browser
    open browser  ${URL}  ${BROWSER}
TC2:See Product
    mouse over  xpath://*[@id="moz-class"]/ul/li[1]/span
TC3:Chose product
    click element  xpath://a[text()='Wakefit Mattress']
TC4:Scroll
    sleep  3
    execute javascript  window.scrollTo(0,400)
   #scroll element into view   xpath://*[@id="WDCM72306"]/div/div/div[2]
TC5:See product PDP
    sleep  3
    click element  xpath://a[text()='Dual Comfort Mattress']
TC6:Add to cart
    sleep  3
    execute javascript  window.scrollTo(0,300)
#    sleep  3
#   # click element  xpath://div[@id='addToCartMob' and text()='Add to cart']
#    click element  xpath://*[@id="addToCartMob"]
#    #scroll element into view  xpath://div[@id='addToCartMob']
